package exam;

import java.util.Scanner;

public class Task14 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int i = 0;
        int silnia = 1;
        System.out.println(silniaWhile(n, i, silnia));
        System.out.println(silniaFor(n, silnia));
        System.out.println(silniaRekurencja(n));

    }

    //while
    public static int silniaWhile(int n, int i, int silnia) {
        while (i < n) {
            i++;
            silnia = silnia * i;
        }
        return silnia;
    }

    //for
    public static int silniaFor(int n, int silnia) {
        for (int i = 1; i <= n; i++) {
            silnia = silnia * i;
        }
        return silnia;
    }

    //rekurencja
    public static int silniaRekurencja(int n) {
        if (n > 1) {
            return n * (silniaRekurencja(n - 1));
        } else return 1; // rekurencja została wygooglana
    }


}
