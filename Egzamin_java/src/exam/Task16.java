package exam;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.LinkedList;

public class Task16 {
    public static void main(String[] args) {
        ArrayList<Integer> arrayList = new ArrayList<>();

        LinkedList<Integer> linkedList = new LinkedList<>();

        Instant start = Instant.now();
        for (int i = 0; i < 100000; i++) {
            arrayList.add(0, i);
        }
        Instant end = Instant.now();
        System.out.println(Duration.between(start, end) + " ArrayList");

        start = Instant.now();
        for (int i = 0; i < 100000; i++) {
            linkedList.add(0, i);
        }
        end = Instant.now();
        System.out.println(Duration.between(start, end) + " LinkedList");

    }

    //wygooglane mierzenie czasu
}

